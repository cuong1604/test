<?php
// Kết nối đến cơ sở dữ liệu
$servername = "localhost"; 
$username = "root"; 
$password = "root"; 
$dbname = "login_register"; 
$port = 3307;

$conn = new mysqli($servername, $username, $password, $dbname, $port);

// Kiểm tra kết nối
if ($conn->connect_error) {
    die("Kết nối thất bại: " . $conn->connect_error);
}

function checkIfIdExists($id) {
    // Thực hiện kiểm tra trong cơ sở dữ liệu
    $id = $_POST["id"];
    $myQuery = "SELECT * FROM users WHERE id = '$id'";
    global $conn;
    $resultQuery = $conn->query($myQuery);
    
    if ($resultQuery->num_rows > 0) {
        return true;
    }else{
        return false;
    }
}

// Xử lý form đăng ký khi nhấn nút "Đăng ký"
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $id = $_POST["id"];

    if (checkIfIdExists($id)) {
        $error_id = 'ID đã tồn tại. Vui lòng nhập ID khác.';
        header('Location: register.php?error='.$error_id);
        exit();
    }

    $password = trim($_POST["password"]);
    $confirmPassword = trim($_POST["confirmPassword"]);
    $hashedPassword = password_hash($password, PASSWORD_DEFAULT);
    $name =$_POST["name"];
    $email = $_POST["email"];
    $phonenumber = $_POST["phonenumber"];

    //Kiểm tra xác minh mật khẩu
    if (strcasecmp($password, $confirmPassword) === 0) {
         //Thực hiện truy vấn để lưu thông tin đăng ký vào cơ sở dữ liệu
         $sql = "INSERT INTO users (id, password, name, email, phonenumber) VALUES ('$id', '$hashedPassword', '$name', '$email', '$phonenumber')";

         if ($conn->query($sql) === TRUE) {
            //  echo "Đăng ký thành công!";
            $successMessage = "Đăng ký thành công! <a href='login.php'>Click để đăng nhập</a>";
            echo $successMessage;
         } else {
             echo "Lỗi: " . $sql . "<br>" . $conn->error;
         }
    } else {
        echo "Xác minh mật khẩu không khớp!";
    }
}

$conn->close();
?>