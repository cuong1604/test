<?php

// Kết nối đến cơ sở dữ liệu
$servername = "localhost";
$username = "root";
$password = "root";
$dbname = "login_register";
$port = 3307;

$conn = new mysqli($servername, $username, $password, $dbname, $port);
// Kiểm tra kết nối
if ($conn->connect_error) {
    die("Kết nối thất bại: " . $conn->connect_error);
}

// Kiểm tra xem người dùng đã truy cập trang action-login.php trực tiếp hay không
if ($_SERVER['HTTP_REFERER'] !== 'http://192.168.1.11/test/login.php') {
    header("Location: login.php");
    exit();
}
// var_dump($conn);die();

// Xử lý form đăng nhập khi nhấn nút "Đăng nhập"
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $idOrEmail = $_POST["id_or_email"];
    $password = $_POST["password"];

    $sql = "SELECT * FROM users WHERE id = '$idOrEmail' OR email = '$idOrEmail'";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        $hashedPassword = $row['PASSWORD'];

        // Kiểm tra xem mật khẩu nhập vào khớp với mật khẩu đã mã hóa hay không
        if (password_verify($password, $hashedPassword)) {
            // header("Location: display.php");
            // exit();
            // echo "Đăng nhập thành công!<br>";
            // echo "ID: " . $row["ID"] . "<br>";
            // echo "Name: " . $row["NAME"] . "<br>";
            // echo "Email: " . $row["email"] . "<br>";
            // echo "Phone Number: " . $row["phonenumber"] . "<br>";
        } else {
            // Mật khẩu sai
            $errorMessage = "Sai thông tin đăng nhập! Vui lòng nhập lại.";
            header("Location: login.php?error=$errorMessage");
            exit();
        }
    } else {
        $errorMessage = "Sai thông tin đăng nhập! Vui lòng nhập lại.";
        header("Location: login.php?error=$errorMessage");
        exit();
    }
}
// Gỡ bỏ bộ nhớ cache của trình duyệt
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Expires: Sat, 1 Jan 2000 00:00:00 GMT");
header("Pragma: no-cache");

$conn->close();
?>

<!-- Hiển thị thông tin người dùng -->

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Thông tin người dùng</title>
    <link rel="stylesheet" type="text/css" href="css/action-login.css">
</head>

<body>
    <div class="container">
        <div class="header">
            <h2 class="title">Đăng nhập thành công!!</h2>
        </div>
        <div class="main-body">
            <div class="inf-user">
                <p class="row">ID : <?php echo "<span class='inp'>" . $row["ID"] . "</span>"; ?></p>
                <p class="row">Họ tên : <?php echo "<span class='inp'>" . $row["NAME"] . "</span>"; ?></p>
                <p class="row">Email : <?php echo "<span class='inp'>" . $row["email"] . "</span>"; ?></p>
                <p class="row">Số điện thoại : <?php echo "<span class='inf'>" . $row["phonenumber"] . "</span>"; ?></p>
            </div>
        </div>
        <!-- <div>
            <form action="login.php"></form>
          <input class="button-logout" type="submit" value="Đăng xuất">
        </div> -->
        <button onclick="logout()">Đăng xuất</button>
    </div>
</body>
<script>
    function logout() {
        window.location.href = "login.php";
    }
</script>

</html>