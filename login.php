<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Đăng nhập</title>
    <link rel="stylesheet" type="text/css" href="css/login.css">
</head>

<body>
    <div class="container">
        <div class="user-layout">
            <div class="user-header">
                <div class="header-wrap">
                    <div class="title-small">Bạn cần tìm việc?</div>
                    <div class="title-big"> Đăng nhập làm Ứng viên</div>
                </div>
            </div>
        </div>
        <div class="user-body">
            <div class="body-form">
                <div class="form-wrap">
                    <div class="form-inner">
                        <form action="action-login.php" method="POST">
                            <div class="inp-wrap">
                                <div class="inp-inner">
                                    <input class="inf-input" type="text" name="id_or_email" placeholder="Nhập ID hoặc Email" required>
                                </div>
                                <div class="inp-inner">
                                    <input class="inf-input" type="password" name="password" placeholder="Nhập mật khẩu" required>
                                </div>
                            </div>
                            <div class="btn-wrap">
                                <button class="btn big-full blue bor-r" type="submit">Đăng nhập</button>
                            </div>
                        </form>
                        <!-- <ul class="list-tit">
                            <li><a href="">Quên ID</a></li>
                            <img src="icons/line.svg" alt="Icon">
                            <li><a href="">Quên mật khẩu</a></li>
                            <img src="icons/line.svg" alt="Icon">
                            <li><a href="register.php">Đăng ký</a></li>
                        </ul> -->
                    </div>
                </div>
                <div class="class-tit">
                    <ul class="list-tit-space">
                        <li><a href="">Quên ID</a></li>
                        <li><a href="">Quên mật khẩu</a></li>
                        <li><a href="register.php">Đăng ký</a></li>
                    </ul>
                </div>
                <?php
                        // Kiểm tra xem có thông báo lỗi không
                        if (isset($_GET['error'])) {
                            $errorMessage = $_GET['error'];
                            echo "<p class='error-message'>$errorMessage</p>";
                        }
                        ?>
            </div>
        </div>
    </div>
</body>

</html>