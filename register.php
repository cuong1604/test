<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Đăng ký</title>
    <link rel="stylesheet" type="text/css" href="css/register.css">
</head>

<body>
    <div class="container">
        <div class="user-layout">
            <div class="user-header">
                <div class="header-wrap">
                    <div class="header-inner">
                        <button class="btn btn-back" onclick="goBack()">
                            <img src="icons/back.svg" alt="Icon">
                        </button>
                        <h1 class="tit-area">Ứng viên đăng ký</h1>
                        <button class="btn btn-back">
                            <img src="icons/home.svg" alt="Icon">
                        </button>
                        <button class="btn btn-back">
                            <img src="icons/setting.svg" alt="Icon">
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="user-body">
            <div class="body-form">
                <div class="form-wrap">
                    <div class="form-inner">
                        <form action="action-register.php" method="POST">
                            <div class="inp-wrap ty">
                                <div class="inp-inner">
                                    <div class="tit">ID</div>
                                    <div class="content">
                                        <input type="text" class="inp" name="id" placeholder="Nhập ID" />
                                    </div>
                                    <div>
                                        <?php if (!empty($_GET['error'])) { ?>
                                            <span class="error-register"><?= $_GET['error'] ?></span>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="inp-inner">
                                    <div class="tit">Mật khẩu</div>
                                    <input type="password" class="inp" name="password" placeholder="Nhập mật khẩu" />
                                </div>
                                <div class="inp-inner">
                                    <div class="tit">Nhập lại mật khẩu</div>
                                    <input type="password" class="inp" name="confirmPassword" placeholder="Xác nhận mật khẩu" />
                                </div>
                            </div>
                            <div class="inp-wrap">
                                <div class="inp-inner">
                                    <div class="tit">Tên</div>
                                    <input type="text" class="inp" name="name" placeholder="Tên" />
                                </div>
                                <div class="inp-inner">
                                    <div class="tit">Email</div>
                                    <input type="text" class="inp" name="email" placeholder="Email@nhập_trực_tiếp" />
                                </div>
                                <div class="inp-inner">
                                    <div class="tit">Điện thoại</div>
                                    <input type="text" class="inp" name="phonenumber" placeholder="Chỉ nhập số" />
                                </div>
                            </div>
                            <div class="btn-wrap">
                                <button type="submit" class="btn big-full blue bor-r"> Đăng ký</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function goBack() {
            window.location.href = "login.php";
        }
    </script>
</body>

</html>